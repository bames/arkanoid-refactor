package pl.kubie;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Ball {

  private static final int SIZE = 10;
  private static final Color COLOR = Color.green;

  private Point position;

  private int directionX = -1;
  private int directionY = -2;

  Ball() {
    int x = (int) (Math.random() * 500);
    int y = 350;
    this.position = new Point(x, y);
  }

  public void draw(Graphics g) {
    g.setColor(COLOR);
    g.fillOval(position.x, position.y, SIZE, SIZE);
  }

  public void move() {
    this.position.x += directionX;
    this.position.y += directionY;

    bounceFromEdges();

  }

  private void bounceFromEdges() {
    if (position.x < 0) {
      invertDirectionX();
    }
    if (position.x > 670) {
      invertDirectionX();
    }
    if (position.y < 0) {
      invertDirectionY();
    }
  }

  public void bounceFromPlayer() {
    this.directionY = -this.directionY;
  }


  public void bounce(Brick brick) {
    if(position.x + 19 <= brick.rectangle().x || position.x +1 >= brick.rectangle().x + brick.rectangle().width) {
      invertDirectionX();
    }
    else {
      invertDirectionY();
    }
  }

  public boolean isOut() {
    return position.y > 570;
  }

  public void stop() {
    this.directionX = 0;
    this.directionY = 0;
  }

  public boolean touch(Player player) {
    return current()
        .intersects(player.rectangle());
  }

  public boolean touch(Brick b) {
    return current().intersects(b.rectangle());
  }

  public void moveToStart() {
    this.position.x = 0;
    this.position.y = 0;
  }

  private Rectangle current() {
    return new Rectangle(position.x, position.y, SIZE, SIZE);
  }

  private void invertDirectionX() {
    this.directionX = -directionX;
  }

  private void invertDirectionY() {
    this.directionY = -directionY;
  }
}
