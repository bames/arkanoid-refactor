package pl.kubie;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Player {

  private static final Color COLOR = Color.green;
  private static final Dimension dimension = new Dimension(100, 8);
  private static final int POSITION_Y = 550;
  private static final int STEP = 20;
  private int position;


  Player() {
    this.position = 310;
  }


  void draw(Graphics graphics) {
    graphics.setColor(COLOR);
    graphics.fillRect(position, POSITION_Y, dimension.width, dimension.height);
  }

  void moveLeft() {
    if (position > 10) {
      position -= STEP;
    }
  }

  void moveRight() {
    if (position < 600) {
      position += STEP;
    }
  }

  Rectangle rectangle() {
    return new Rectangle(position, POSITION_Y, dimension.width, dimension.height);
  }
}
