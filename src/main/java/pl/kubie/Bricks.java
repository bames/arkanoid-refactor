package pl.kubie;


import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Bricks {

  private static final int WIDTH = 540;
  private static final int HEIGHT = 150;
  private static final int WIDTH_OFFSET = 80;
  private static final int HEIGHT_OFFSET = 50;

  private List<Brick> bricks;

  Bricks(int rows, int columns) {
    int brickWidth = WIDTH / columns;
    int brickHeight = HEIGHT / rows;

    bricks = new ArrayList<>();

    for (int row = 0; row < rows; row++) {
      for (int column = 0; column < columns; column++) {
        int x = column * brickWidth + WIDTH_OFFSET;
        int y = row * brickHeight + HEIGHT_OFFSET;
        Brick brick = new Brick(
            new Point(x, y),
            new Dimension(brickWidth, brickHeight)
        );
        bricks.add(brick);
      }
    }
  }

  public void draw(Graphics2D graphics) {
    bricks.forEach(b -> b.draw(graphics));
  }

  public int handle(Ball ball, int score) {
    return bricks.stream()
        .filter(b -> b.isActive() && ball.touch(b))
        .map((Brick b) -> {
          b.disable();
          ball.bounce(b);
          return score;
        })
        .reduce(0, Integer::sum);
  }

  public boolean outOfBricks() {
    return this.activeBricks() <= 0;
  }

  private long activeBricks() {
    return bricks.stream()
        .filter(Brick::isActive)
        .count();
  }
}

