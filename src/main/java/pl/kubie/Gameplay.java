package pl.kubie;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Gameplay extends JPanel implements KeyListener, ActionListener {

  private static final int DELAY = 6;
  private static final int BACKGROUND_WIDTH = 692;
  private static final int BACKGROUND_HEIGHT = 592;
  private static final int SUCCESS_SCORE = 5;
  private final Timer timer;

  private Player player;
  private Bricks bricks;
  private Ball ball;
  private int score;

  Gameplay() {
    setupPanel();
    timer = new Timer(DELAY, this);
    initGame();
  }

  @Override
  public void paint(Graphics g) {
    setBackground(g);
    drawFrame(g);
    printScore(g);
    bricks.draw((Graphics2D) g);
    ball.draw(g);
    player.draw(g);
    handleFail(g);
    handleWin(g);
    g.dispose();
  }

  @Override
  public void actionPerformed(ActionEvent e) {

    if (ball.touch(player)) {
      ball.bounceFromPlayer();
    }
    score += bricks.handle(ball, SUCCESS_SCORE);

    ball.move();

    repaint();
  }

  @Override
  public void keyPressed(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
      player.moveRight();
    }
    if (e.getKeyCode() == KeyEvent.VK_LEFT) {
      player.moveLeft();
    }
    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
      initGame();
      repaint();
    }

  }

  private void setupPanel() {
    addKeyListener(this);
    setFocusable(true);
    setFocusTraversalKeysEnabled(false);
  }

  private void handleFail(Graphics g) {
    if (ball.isOut()) {
      ball.stop();
      ball.moveToStart();
      printFailEndMessage(g);
      timer.stop();
    }
  }


  private void handleWin(Graphics g) {
    if (bricks.outOfBricks()) {
      ball.moveToStart();
      printWinEndMessage(g);
      timer.stop();
    }
  }

  private void printFailEndMessage(Graphics g) {
    g.setColor(Color.RED);

    g.setFont(new Font("serif", Font.BOLD, 30));
    g.drawString("Koniec gry", 280, 300);

    g.setFont(new Font("serif", Font.BOLD, 20));
    g.drawString("Wciśnij Enter aby zresetować", 230, 350);
  }


  private void initGame() {
    ball = new Ball();
    player = new Player();
    bricks = new Bricks(3, 7);
    score = 0;
    timer.start();
  }

  private void printWinEndMessage(Graphics g) {
    g.setColor(Color.RED);
    g.setFont(new Font("serif", Font.BOLD, 30));
    g.drawString("Zwycięstwo:", 260, 300);

    g.setFont(new Font("serif", Font.BOLD, 20));
    g.drawString("Wciśnij Enter aby zresetować", 230, 350);
  }

  private void printScore(Graphics g) {
    g.setColor(Color.white);
    g.setFont(new Font("serif", Font.BOLD, 25));
    g.drawString("" + score, 590, 30);
  }

  private void drawFrame(Graphics g) {
    g.setColor(Color.yellow);
    g.fillRect(0, 0, 3, 592);
    g.fillRect(0, 0, 692, 3);
    g.fillRect(691, 0, 3, 592);
  }

  private void setBackground(Graphics g) {
    g.setColor(Color.black);
    g.fillRect(1, 1, BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
  }


  @Override
  public void keyTyped(KeyEvent e) {

  }

  @Override
  public void keyReleased(KeyEvent e) {

  }

}