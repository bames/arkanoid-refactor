package pl.kubie;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

public class Brick {

  private static final BasicStroke STROKE = new BasicStroke(3);
  private static final Color LIGHT_GRAY = Color.lightGray;
  private static final Color BLACK = Color.black;

  private boolean active;
  private Point position;
  private Dimension dimension;

  Brick(Point position, Dimension dimension) {
    this.position = position;
    this.dimension = dimension;
    this.active = true;
  }

  boolean isActive() {
    return active;
  }

  void disable() {
    this.active = false;
  }

  void draw(Graphics2D graphics) {
    if (isActive()) {
      graphics.setColor(Color.GRAY);
      graphics.fillRect(
          position.x,
          position.y,
          dimension.width,
          dimension.height
      );

      graphics.setStroke(new BasicStroke(3));
      graphics.setColor(Color.black);
      graphics.drawRect(position.x,
          position.y,
          dimension.width,
          dimension.height
      );
    }
  }

  Rectangle rectangle() {
    return new Rectangle(position, dimension);
  }
}
